import React, { useState } from 'react';
import MovieForm from './Components/MovieForm';
import MovieList from './Components/MovieList';
import MovieCard from './Components/MovieCard';

const App = () => {
  const [films, setFilms] = useState([]);
  const updateForm = (film) => setFilms([...films, film])
  return (
    <div>
      <div className='col-3'>
        <MovieForm updateForm={updateForm}/>    
      </div>
      <hr />
      <div className="row">
        <div className="col-5">
          <MovieList film={films}/>
        </div>
        <div className="col-7 d-flex cardFilm justify-content-center">
          <MovieCard film={films}/>
        </div>
      </div>
    </div>
  );
}

export default App;

import React from 'react';

const MovieCard = ({film}) => {
    return (
        <>
            {film && film.map((item, index) => 
                // <div className="col-5">
                    <div className="card m-3" key={index} >
                        <div className="card-body">
                            <h5 className="card-title">{item.filmTitle}</h5>
                            <p className="card-text">{item.description} </p>
                            <p className="card-text">{item.rating}/10 </p>
                            {/* <a href="#" className="btn btn-primary">Go somewhere</a> */}
                        </div>
                    </div>
                // </div>
            )}
        </>
    );
}

export default MovieCard;

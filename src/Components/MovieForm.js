import React, { useState } from 'react';

const MovieForm = ({updateForm}) => {
    const [validated, setValidated] = useState(false);

    const [myForm, setMyForm] = useState({
        filmTitle: '',
        description: '',
        rating: ''

    })

    const handleChange = (e) => (
        setMyForm({...myForm, [e.target.name]: e.target.value})
    )

    const handleSubmit = (e) => {
        e.preventDefault();
        const formulaire = e.currentTarget;

        if (formulaire.checkValidity() === false){
            e.stopPropagation();
        } else{
            setValidated(true);
            updateForm(myForm);
            setMyForm({
                filmTitle: '',
                description: '',
                rating: ''
            })
        }
    }

    return (
        <div>
            <h2>Add New</h2>
            <form class="row g-3" validated={validated} onSubmit={handleSubmit} >
                <div class="col-12">
                    <input type="text" class="form-control" name="filmTitle" id="filmTitle" placeholder='Film Title' value={myForm.filmTitle} required onChange={handleChange} />
                </div>
                <div class="col-12">
                    <textarea class="form-control" name="description" id="description" placeholder='Enter Film description' value={myForm.description} required onChange={handleChange} />
                </div>
                <div class="col-12">
                    <input type='number' class="form-control" min={1} max={10} name="rating" id="rating" aria-describedby="inputGroupPrepend2" value={myForm.rating} required onChange={handleChange} />
                </div>
                <div class="col-12">
                    <button class="btn btn-primary" type="submit">Submit form</button>
                </div>
                </form>
        </div>
    );
}

export default MovieForm;

import React from 'react';

const MovieList = ({film}) => {
    return (
        <div className='px-3'>
            <table className="table table-striped table-hover">
                <thead>
                    <tr>
                    <th scope="col">#</th>
                    <th scope="col">Title</th>
                    <th scope="col">Description</th>
                    <th scope="col">Rating</th>
                    </tr>
                </thead>
                <tbody>
                    {film && film.map((item, index) => 
                        <tr key={index}>
                        <th scope="row">{index+1}</th>
                        <td>{item.filmTitle}</td>
                        <td>{item.description}</td>
                        <td>{item.rating}/10</td>
                        </tr>
                    )}
                </tbody>
            </table>
        </div>
    );
}

export default MovieList;
